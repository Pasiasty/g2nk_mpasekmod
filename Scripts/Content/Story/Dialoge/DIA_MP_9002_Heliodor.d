//////////////////////////////////////////////////////////////////////
//	Info EXIT 
///////////////////////////////////////////////////////////////////////
INSTANCE DIA_Heliodor_EXIT   (C_INFO)
{
	npc         = VLK_9002_Heliodor;
	nr          = 999;
	condition   = DIA_Heliodor_EXIT_Condition;
	information = DIA_Heliodor_EXIT_Info;
	permanent   = TRUE;
	description = DIALOG_ENDE;
};

FUNC INT DIA_Heliodor_EXIT_Condition()
{
	return TRUE;
};

FUNC VOID DIA_Heliodor_EXIT_Info()
{
	AI_StopProcessInfos (self);
};

//////////////////////////////////////////////////////////////////////
//	Getting rock quest
///////////////////////////////////////////////////////////////////////

INSTANCE DIA_Heliodor_GettingRockQuest   (C_INFO)
{
	npc         = VLK_9002_Heliodor;
	nr          = 1;
	condition   = DIA_Heliodor_GettingRockQuest_Condition;
	information = DIA_Heliodor_GettingRockQuest_Info;
	permanent   = FALSE;
	description = "Wygladasz na zmartwionego.";
};

FUNC INT DIA_Heliodor_GettingRockQuest_Condition()
{	
	return TRUE;
};

FUNC VOID DIA_Heliodor_GettingRockQuest_Info()
{			
	AI_Output (other, self, "DIA_Heliodor_GettingRockQuest_15_00");//Wygladasz na zmartwionego.
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_01_01");//Tak, nigdzie nie mog� znale�� swojego kamienia.
	AI_Output (other, self, "DIA_Heliodor_GettingRockQuest_15_02");//Kamienia?
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_01_03");//Tak. Wczoraj z czystej g�upoty poszed�em rozejrze� si� po tej wielkiej wie�y niedaleko stad
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_01_04");//i nagle us�ysza�em jakie� dziwne warczenie. Zaraz potem szybko stamtad uciek�em.
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_01_05");//Zdaje si�, �e nic mnie nie goni�o, ale z kieszeni musia� mi gdzie� wypa�� m�j szcz�liwy kamie�.
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_01_06");//Mo�e m�g�by� go dla mnie poszuka�?
	
	Info_ClearChoices (DIA_Heliodor_GettingRockQuest);	
	Info_AddChoice (DIA_Heliodor_GettingRockQuest,"Nie mam czasu na takie rzeczy.",DIA_Heliodor_GettingRockQuest_Decline);
	Info_AddChoice (DIA_Heliodor_GettingRockQuest,"W porzadku.",DIA_Heliodor_GettingRockQuest_Accept);
};

FUNC VOID DIA_Heliodor_GettingRockQuest_Decline()
{
	AI_Output (other, self, "DIA_Heliodor_GettingRockQuest_Decline_15_00");//Nie mam czasu na takie rzeczy.
	AI_StopProcessInfos (self);
};

FUNC VOID DIA_Heliodor_GettingRockQuest_Accept()
{
	AI_Output (other, self, "DIA_Heliodor_GettingRockQuest_Accept_15_00");//Niech b�dzie. Znajd� tw�j cenny kamie�.
	AI_Output (self, other, "DIA_Heliodor_GettingRockQuest_Accept_01_01");//Dzi�kuj�. Bardzo Ci dzi�kuj�.
	
	Wld_InsertItem (ItMi_HeliodorsRock, "NW_XARDAS_TOWER_IN1_22"); 
	
	LOG_CreateTopic (TOPIC_HeliodorRockQuest, LOG_MISSION);
	LOG_SetTopicStatus (TOPIC_HeliodorRockQuest, LOG_RUNNING);
	B_LogEntry (TOPIC_HeliodorRockQuest, "Prawdopodobnie gdzie� w wie�y Xardasa znajd� kamie�, kt�rego szuka Heliodor.");
	
	AI_StopProcessInfos (self);
};

//////////////////////////////////////////////////////////////////////
//	Finishing rock quest
///////////////////////////////////////////////////////////////////////

INSTANCE DIA_Heliodor_FinishingRockQuest   (C_INFO)
{
	npc         = VLK_9002_Heliodor;
	nr          = 1;
	condition   = DIA_Heliodor_FinishingRockQuest_Condition;
	information = DIA_Heliodor_FinishingRockQuest_Info;
	permanent   = FALSE;
	description = "Znalaz�em tw�j kamie�.";
};

FUNC INT DIA_Heliodor_FinishingRockQuest_Condition()
{	
	if (Npc_HasItems (other, ItMi_HeliodorsRock ) >= 1)
	{	
		return TRUE;
	};
};

FUNC VOID DIA_Heliodor_FinishingRockQuest_Info()
{			
	AI_Output (other, self, "DIA_Heliodor_FinishingRockQuest_15_00");//Znalaz�em tw�j kamie�.
	AI_Output (self, other, "DIA_Heliodor_FinishingRockQuest_01_01");//O rety. Naprawd� tam poszed�e�. Dzi�kuj� ci bardzo. Je�li b�dziesz kiedy� czego� potrzebowa�, to wiesz gdzie mnie znale��.
	
	B_GiveInvItems (other, self,ItMi_HeliodorsRock, 1);
	
	B_LogEntry (TOPIC_HeliodorRockQuest, "Zanios�em kamie� Heliodorowi. Wyglada na to, �e naprawd� by� dla niego wa�ny.");
	LOG_SetTopicStatus (TOPIC_HeliodorRockQuest, LOG_SUCCESS);
	B_GivePlayerXP (300);
};

//////////////////////////////////////////////////////////////////////
//	Ask about Krzesimir
///////////////////////////////////////////////////////////////////////

INSTANCE DIA_Heliodor_AskAboutKrzesimir   (C_INFO)
{
	npc         = VLK_9002_Heliodor;
	nr          = 1;
	condition   = DIA_Heliodor_AskAboutKrzesimir_Condition;
	information = DIA_Heliodor_AskAboutKrzesimir_Info;
	permanent   = FALSE;
	description = "Pozna�e� mo�e Krzesimira?";
};

FUNC INT DIA_Heliodor_AskAboutKrzesimir_Condition()
{	
	if (Npc_KnowsInfo (other, DIA_Krzesimir_Hallo))
	{	
		return TRUE;
	};
};

FUNC VOID DIA_Heliodor_AskAboutKrzesimir_Info()
{			
	AI_Output (other, self, "DIA_Heliodor_AskAboutKrzesimir_15_00");//Pozna�e� mo�e Krzesimira?
	AI_Output (self, other, "DIA_Heliodor_AskAboutKrzesimir_01_01");//Tak, spotka�em go par� razy. Strasznie nachalny typ, co chwil� wypytuje ludzi, kt�rych spotka o przer�ne rzeczy.
	AI_Output (self, other, "DIA_Heliodor_AskAboutKrzesimir_01_02");//Szczerze m�wiac nie przepadam za nim zbytnio.
};

//////////////////////////////////////////////////////////////////////
//	Help with joke
///////////////////////////////////////////////////////////////////////

INSTANCE DIA_Heliodor_HelpWithJoke   (C_INFO)
{
	npc         = VLK_9002_Heliodor;
	nr          = 1;
	condition   = DIA_Heliodor_HelpWithJoke_Condition;
	information = DIA_Heliodor_HelpWithJoke_Info;
	permanent   = TRUE;
	description = "(Pomoc z �artem dla Krzesimira)";
};

FUNC INT DIA_Heliodor_HelpWithJoke_Condition()
{	
	if (joke_quest_status == 1
	&& Npc_KnowsInfo (other, DIA_Heliodor_AskAboutKrzesimir))
	{	
		return TRUE;
	};
};

FUNC VOID DIA_Heliodor_HelpWithJoke_Info()
{	
	AI_Output (other, self, "DIA_Heliodor_HelpWithJoke_15_00");//M�g�by� mi pom�c z �artem dla Krzesimira?
	
	if (Npc_KnowsInfo (other, DIA_Heliodor_FinishingRockQuest))
	{
		AI_Output (self, other, "DIA_Heliodor_HelpWithJoke_01_01");//Pewnie �e ci pomog�. O! Widz�, �e masz przy sobie pami�tniczek.
		AI_Output (self, other, "DIA_Heliodor_HelpWithJoke_01_02");//Pozw�l, �e zapisz� ci w nim �art, kt�ry powinien go rozbawi�.
		
		joke_quest_status = 2;		
		B_LogEntry (TOPIC_KrzesimirJoke, "Heliodor zapisa� mi taki �art: Ida dwie �odzie podwodne przez las i jedna m�wi: patrz, telewizor leci. Na to druga: no tak, pewnie musi mie� gdzie� tutaj gniazdko.\n\n Naprawd� nie lubi� jak kto� inny pisze w moim dzienniku.");
	}
	else
	{
		AI_Output (self, other, "DIA_Heliodor_HelpWithJoke_01_03");//Nie znam ci� za bardzo. Chyba nie mam ochoty ci pomaga�.
	};
};
