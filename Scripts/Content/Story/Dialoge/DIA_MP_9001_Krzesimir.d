//////////////////////////////////////////////////////////////////////
//	Info EXIT 
///////////////////////////////////////////////////////////////////////
INSTANCE DIA_Krzesimir_EXIT   (C_INFO)
{
	npc         = VLK_9001_Krzesimir;
	nr          = 999;
	condition   = DIA_Krzesimir_EXIT_Condition;
	information = DIA_Krzesimir_EXIT_Info;
	permanent   = TRUE;
	description = DIALOG_ENDE;
};

FUNC INT DIA_Krzesimir_EXIT_Condition()
{
	return TRUE;
};

FUNC VOID DIA_Krzesimir_EXIT_Info()
{
	AI_StopProcessInfos (self);
};

//////////////////////////////////////////////////////////////////////
//	Info Hallo
///////////////////////////////////////////////////////////////////////
INSTANCE DIA_Krzesimir_Hallo   (C_INFO)
{
	npc         = VLK_9001_Krzesimir;
	nr          = 1;
	condition   = DIA_Krzesimir_Hallo_Condition;
	information = DIA_Krzesimir_Hallo_Info;
	permanent   = FALSE;
	important	= TRUE;
};

FUNC INT DIA_Krzesimir_Hallo_Condition()
{	
	if (Npc_IsInState(self, ZS_Talk))	
	{
		return TRUE;
	};
};

var int joke_quest_status;

FUNC VOID DIA_Krzesimir_Hallo_Info()
{
	AI_Output (other, self, "DIA_Krzesimir_Hallo_15_00");//Co tu robisz?
	AI_Output (self, other, "DIA_Krzesimir_Hallo_01_01");//Czekam na kogo� takiego jak ty.
	AI_Output (other, self, "DIA_Krzesimir_Hallo_15_02");//Co masz na my�li?
	AI_Output (self, other, "DIA_Krzesimir_Hallo_01_03");//Czekam az ktos mi opowie jakis naprawde dobry zart. I pomy�la�em, �e ty sie do tego �wietnie nadasz.
	AI_Output (other, self, "DIA_Krzesimir_Hallo_15_04");//M�g�bym spr�bowa�. Ale co m�glby� mi zaoferowa� w zamian?	
	AI_Output (self, other, "DIA_Krzesimir_Hallo_01_05");//Je�li opowiesz mi �art, kt�ry mnie powali na kolana, to obsypie cie z�otem. Mo�e by�?
		
	Info_ClearChoices (DIA_Krzesimir_Hallo);	
	Info_AddChoice (DIA_Krzesimir_Hallo,"Nie mam czasu na takie rzeczy.",DIA_Krzesimir_JokeQuest_Decline);
	Info_AddChoice (DIA_Krzesimir_Hallo,"W porzadku.",DIA_Krzesimir_JokeQuest_Accept);		
};

FUNC VOID DIA_Krzesimir_JokeQuest_Accept()
{
	AI_Output (other, self, "DIA_Krzesimir_JokeQuest_Accept_15_00");//W porzadku.

	LOG_CreateTopic (TOPIC_KrzesimirJoke, LOG_MISSION);
	LOG_SetTopicStatus (TOPIC_KrzesimirJoke, LOG_RUNNING);
	B_LogEntry (TOPIC_KrzesimirJoke, "Musze wymysli� jakis �wietny �art dla Krzesimira. Podobno ma dla mnie jaka� specjalna nagrode");
	
	joke_quest_status = 1;
	
	AI_StopProcessInfos (self);
};

FUNC VOID DIA_Krzesimir_JokeQuest_Decline()
{
	AI_Output (other, self, "DIA_Krzesimir_JokeQuest_Decline_15_00");//Nie mam czasu na takie rzeczy.

	joke_quest_status = 0;

	AI_StopProcessInfos (self);
};

//////////////////////////////////////////////////////////////////////
//	Info Jokes
///////////////////////////////////////////////////////////////////////

INSTANCE DIA_Krzesimir_Joke   (C_INFO)
{
	npc         = VLK_9001_Krzesimir;
	nr          = 2;
	condition   = DIA_Krzesimir_Joke_Condition;
	information = DIA_Krzesimir_Joke_Info;
	permanent   = TRUE;
	description = "Odnosnie tego zartu...";
};

FUNC INT DIA_Krzesimir_Joke_Condition()
{	
	if (Npc_IsInState(self, ZS_Talk)
	&& Npc_KnowsInfo (other, DIA_Krzesimir_Hallo)
	&& joke_quest_status > 0)
	{
		return TRUE;
	};
};
FUNC VOID DIA_Krzesimir_Joke_Info()
{			
	Info_ClearChoices (DIA_Krzesimir_Joke);	
	Info_AddChoice (DIA_Krzesimir_Joke,"Chyba jeste� za g�upi na jakiekolwiek �arty.", DIA_Krzesimir_Joke_Offend);
	Info_AddChoice (DIA_Krzesimir_Joke,"Przychodzi baba do lekarza...",                DIA_Krzesimir_Joke_2);
	Info_AddChoice (DIA_Krzesimir_Joke,"Przychodzi ork do baru...",                    DIA_Krzesimir_Joke_1);

	if (joke_quest_status == 2) 
	{
		Info_AddChoice (DIA_Krzesimir_Joke,"Ida dwie �odzie podwodne przez las...",    DIA_Krzesimir_HilariousJoke);	
	};
};

FUNC VOID DIA_Krzesimir_Joke_Offend()
{
	AI_Output (other, self, "DIA_Krzesimir_Offend_15_00");//Chyba jeste� za g�upi na jakiekolwiek �arty.
	AI_Output (self, other, "DIA_Krzesimir_Offend_01_01");//Ty gnido! Zaraz oberwiesz!	
	
	joke_quest_status = 0;
		
	B_LogEntry (TOPIC_KrzesimirJoke, "Wyglada na to, �e rozgniewa�em Krzesimira i nie bede mog� mu ju� opowiedzie� dobrego �artu");
	LOG_SetTopicStatus (TOPIC_KrzesimirJoke, LOG_FAILED);
	B_GivePlayerXP (400);
	
	AI_StopProcessInfos (self);
	B_Attack (self, other, AR_NONE, 1);	
};

FUNC VOID DIA_Krzesimir_Joke_1()
{
	AI_Output (other, self, "DIA_Krzesimir_Joke_15_01");//Przychodzi ork do baru i m�wi: dajcie mi co� dla zielonych,
	AI_Output (other, self, "DIA_Krzesimir_Joke_15_02");//a barman mu da� plakietke greanpeace'u.
	AI_Output (self, other, "DIA_Krzesimir_Joke_01_00");//To chyba najgorszy �art jaki w �yciu s�ysza�em.
	
	AI_StopProcessInfos (self);
};

FUNC VOID DIA_Krzesimir_Joke_2()
{
	AI_Output (other, self, "DIA_Krzesimir_Joke_15_03");//Przychodzi baba do lekarza i pyta: 
	AI_Output (other, self, "DIA_Krzesimir_Joke_15_04");//eee... nie pami�tam co by�o dalej.
	AI_Output (self, other, "DIA_Krzesimir_Joke_01_01");//To mo�e przygotuj jaki� �art zanim z nim do mnie przyjdziesz, co?
	
	AI_StopProcessInfos (self);
};

FUNC VOID DIA_Krzesimir_HilariousJoke()
{
	AI_Output (other, self, "DIA_Krzesimir_HilariousJoke_15_00");//Ida dwie �odzie podwodne przez las i jedna m�wi: Patrz, telewizor leci.
	AI_Output (other, self, "DIA_Krzesimir_HilariousJoke_15_01");//A druga na to: no tak, pewnie musi tutaj mie� gdzie� gniazdko.
	AI_Output (self, other, "DIA_Krzesimir_HilariousJoke_01_02");//Ha ha ha ha ha! Umre ze smiechu!	
	AI_Output (other, self, "DIA_Krzesimir_HilariousJoke_15_03");//Ciesze sie, ze Ci sie podoba.
	AI_Output (self, other, "DIA_Krzesimir_HilariousJoke_01_04");//Och, nie moge oddychac.
	AI_Output (other, self, "DIA_Krzesimir_HilariousJoke_15_05");//Wszystko w porzadku?
	AI_Output (self, other, "DIA_Krzesimir_HilariousJoke_01_06");//Czekaj, musze popic.
	
	AI_StopProcessInfos (self);
	
	Createinvitems (self, ItMi_DeathPotion, 1);
	AI_UseItem (self, ItMi_DeathPotion);
	
	B_LogEntry (TOPIC_KrzesimirJoke, "Opowiedzia�em Krzesimirowi �art o �odziach podwodnych. Wyglada na to, �e umar� ze �miechu.");
	LOG_SetTopicStatus (TOPIC_KrzesimirJoke, LOG_SUCCESS);
	B_GivePlayerXP (300);
};
