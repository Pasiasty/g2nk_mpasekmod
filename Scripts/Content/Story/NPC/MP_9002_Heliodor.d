
instance VLK_9002_Heliodor(Npc_Default)
{
	// ------ NSC ------
	name 		= "Heliodor";
	guild 		= GIL_VLK;
	id 			= 9002;
	voice 		= 1;
	flags       = 0;															
	npctype		= NPCTYPE_MAIN;
	
	// ------ Attribute ------
	B_SetAttributesToChapter (self, 3);
	
	// ------ Kampf-Taktik ------
	fight_tactic		= FAI_HUMAN_STRONG;	
	
	// ------ Equippte Waffen ------																	
	EquipItem			(self, ItMw_1H_Mace_L_04);
	
	// ------ Inventory ------
	// H�ndler
	// ------ visuals ------																			
	B_SetNpcVisual 		(self, MALE, "Hum_Head_Bald", Face_P_Weak_Ulf_Wohlers, BodyTex_P, ITAR_Governor);	
	Mdl_SetModelFatness	(self, 0.9);
	Mdl_ApplyOverlayMds	(self, "Humans_Arrogance.mds");
	
	// ------ NSC-relevante Talente vergeben ------
	B_GiveNpcTalents (self);
	
	// ------ Kampf-Talente ------																		
	B_SetFightSkills (self, 35);
	
	// ------ TA anmelden ------
	daily_routine 		= Rtn_Heliodor_9002;
};

FUNC VOID Rtn_Heliodor_9002 ()
{	
	TA_Sleep		(22,00,07,00,"NW_CITY_BED_HARAD"); 
	
	TA_Stand_ArmsCrossed	(07,00,21,00,"NW_HELIODOR");
	TA_Stand_ArmsCrossed	(21,00,07,00,"NW_HELIODOR");
};
