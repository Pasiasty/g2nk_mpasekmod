
instance VLK_9001_Krzesimir (Npc_Default)
{
	// ------ NSC ------
	name 		= "Krzesimir";
	guild 		= GIL_VLK;
	id 			= 9001;
	voice 		= 1;
	flags       = 0;															
	npctype		= NPCTYPE_MAIN;
	
	// ------ Attribute ------
	B_SetAttributesToChapter (self, 3);
	
	// ------ Kampf-Taktik ------
	fight_tactic		= FAI_HUMAN_STRONG;	
	
	// ------ Equippte Waffen ------																	
	EquipItem			(self, ItMw_1H_Mace_L_04);
	
	// ------ Inventory ------
	// H�ndler
	// ------ visuals ------																			
	B_SetNpcVisual 		(self, MALE, "Hum_Head_FatBald", Face_N_NormalBart20, BodyTex_N, ITAR_Smith);	
	Mdl_SetModelFatness	(self, 5);	
	
	// ------ NSC-relevante Talente vergeben ------
	B_GiveNpcTalents (self);
	
	// ------ Kampf-Talente ------																		
	B_SetFightSkills (self, 35);
	
	// ------ TA anmelden ------
	daily_routine 		= Rtn_Krzesimir_9001;
};

FUNC VOID Rtn_Krzesimir_9001 ()
{	
	TA_Sleep		(22,00,07,00,"NW_CITY_BED_HARAD"); 
	
	TA_Stand_Eating	(07,00,21,00,"NW_KRZESIMIR");
	TA_Stand_Eating	(21,00,07,00,"NW_KRZESIMIR");
};
